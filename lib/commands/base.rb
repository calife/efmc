module Commands

  VERSION='2015-07-23'

=begin
Base class for command
=end
  class BaseCommand

    include Logging

    @command=nil # Command
    @global=nil # Global option
    @options=nil # Local option
    @args=nil # arguments

    def initialize(arguments)
      @command, @global, @options, @args = arguments
    end

    def execute
      logger.debug("Starting" + self.class.to_s + "___" + __method__.to_s+" ...")

      
      if self.respond_to?(@command)
        
        logger.debug("Executing command #{@command} for class #{self.class}")

        begin

          self.send(@command)

        rescue Exception => ex # rescue centrale id gestione delle eccezioni, i command devono solo sollevare le eccezioni
          logger.error ex.message
          ex.backtrace.each { |line|
            logger.error line
          }          
          raise ex
        end
        
      else
        logger.error("Command #{@command} for class #{self.class} does not exists!")
        raise "Unknown Command Exception"
      end
      
      logger.debug("...leaving" + self.class.to_s + "___" + __method__.to_s)      
    end
    
  end

end
