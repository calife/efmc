module Commands

  class Go2Command < Commands::BaseCommand

    def go2

      appname=@args.first

      if Commands::ALIASES_CMD.key?(appname.to_sym)

        command = Commands::ALIASES_CMD.select { |key, value| key.to_s.match(appname) }

        logger.debug( "#{appname} aliased to \" #{command[appname.to_sym]} \" " )
        
        system( command[appname.to_sym] )
         
      else
        msg=" Appname not found or no appname provided."
        raise msg
      end
      
    end
    
  end
  
end
