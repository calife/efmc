# coding: utf-8
module Commands

  class SSHKeyCommand < Commands::BaseCommand
     
=begin
    Aggiunge la chiave pubblica ssh nel file ~/.ssh/authorized_keys dell' account remoto
    Once you create your keys, you then need to set up your account on each remote server so that it knows about your public key.
    To do this, log into the remote server and edit (or create) the file (in your home directory) ”.ssh/authorized_keys”.
    Just copy the contents of your public key (in your local machine’s home directory, called ”.ssh/id_dsa.pub” or ”.ssh/id_rsa.pub”)
    into the “authorized_keys” file on a line of its own. Then save the file and logout. Everything should now be set up.

    @since Wednesday, 12. August 2015
=end    
    def sshkey_add()

      logger.debug("Starting" + self.class.to_s + "___" + __method__.to_s+" ...")

      username,host=Commands::Utils.decode_args(@args.first)

      filename=@options[:filename]
      filename.sub! '$HOME', ENV["HOME"]

      unless Commands::Utils.valid_ip_address?(host)
        msg="Host #{host} is not a valid network address"
        raise msg
      end
      unless  File.exist?(filename)
        msg="File #{filename} does not exists"
        raise msg
      end

      key_added=pri_addkey(host,username,filename)
      if key_added
        printf "%s\n" , "key #{filename} added."
      else
        printf "%s\n" , "Nothing to add, key #{filename} already exists."
      end
      
    end

=begin
    Rimuove la chiave pubblica ssh dal file ~/.ssh/authorized_keys dell' account remoto
    @since Wednesday, 12. August 2015
=end    
    def sshkey_remove()
      logger.debug("Starting" + self.class.to_s + "___" + __method__.to_s+" ...")

      username,host=Commands::Utils.decode_args(@args.first)
     
      filename=@options[:filename]
      filename.sub! '$HOME', ENV["HOME"]
      
      unless Commands::Utils.valid_ip_address?(host)
        msg= "#{host} is not a valid network address"
        raise msg
      end

      unless  File.exist?(filename)
        msg="File #{filename} does not exists"
        raise msg
      end
      
      key_removed=pri_removekey(host,username,filename)
      if key_removed
        printf "%s\n" , "key #{filename} removed"
      else
        printf "%s\n" , "key #{filename} not found, nothing to remove"
        
      end

      logger.debug("...leaving" + self.class.to_s + "___" + __method__.to_s)
    end


=begin
    Elenca le chiavi pubbliche ssh dal file ~/.ssh/authorized_keys dell' account remoto
    @since Wednesday, 12. August 2015
=end    
    def sshkey_list()
      logger.debug("Starting" + self.class.to_s + "___" + __method__.to_s+" ...")

      username,host=Commands::Utils.decode_args(@args.first)

      unless Commands::Utils.valid_ip_address?(host)
        msg= "Host #{host} is not a valid network address"
        raise msg
      end

      local_fingerprint=getLocalFingerprint() # fingerprint delle chiavi locali
      keys_enabled=pri_listkeys(host,username) # fingerprint delle chiavi remote
      
      if !keys_enabled.nil? && keys_enabled.size>0
        keys_enabled.each { |x|
          fing=SSHKey.fingerprint x
          printf "Public keys enabled to login: %s" , "#{fing}"
          if local_fingerprint.key(fing)
            printf " match your public key file %s " , "#{local_fingerprint.key(fing)}"
          end
          printf "\n"
        }
      else
        msg="No keys found"
        printf "%s\n" ,msg
      end
      
      logger.debug("...leaving" + self.class.to_s + "___" + __method__.to_s+" ...")
      
    end
    

    private
=begin
Utilizza rye
=end
  def pri_addkey(remote_host,remote_user,local_pubkey_file)

    local_pubkey= File.open(local_pubkey_file, 'rb') { |f| f.read }
    local_pubkey.chomp!

    unless SSHKey.valid_ssh_public_key? local_pubkey
      msg= "File #{local_pubkey} does not contains a valid ssh key"
      raise msg
    end

    final_keys=[local_pubkey]
    remote_keys_arr_size=0

    password=Commands::Utils.ask_password("Enter password for #{remote_user}@#{remote_host}")
    rbox = Rye::Box.new(remote_host,:user => remote_user,:password=>password)
    
    str_home= rbox.pwd
    ssh_dir="#{str_home}"+"/.ssh"
    if ! rbox.file_exists?(ssh_dir) # create directory ~/.ssh
      rbox.mkdir(ssh_dir)
      rbox.chmod("700",ssh_dir)
    else # directory ~/.ssh exists
      
      remote_keys_exists=rbox.file_exists?("#{ssh_dir}"+"/authorized_keys")
      if remote_keys_exists # file authorized exists then may be that authorized keys exists

        remote_keys=rbox.string_download ("#{ssh_dir}"+"/authorized_keys")
        remote_keys_arr=remote_keys.lines
        remote_keys_arr_size=remote_keys_arr.size

        remote_keys_arr.each { |x|
          final_keys.push(x.chomp)
        }
        
      end

    end

    final_keys.uniq!

    applejack=StringIO.new(final_keys.join("\n"))
    rbox.file_upload applejack, "#{ssh_dir}/authorized_keys"
    rbox.chmod "644","#{ssh_dir}/authorized_keys"

    return (remote_keys_arr_size+1==final_keys.size)
    
  end

=begin
Rimuove la chiave pubblica dal file .ssh/authorized_keys remoto.
E' necessario procedere attraverso un compare dell' sshkey fingerprint (per evitare escape di caratteri contenuti all' interno della chiave pubblica)
=end
  def pri_removekey(remote_host,remote_user,local_pubkey_file)

    result=false # key found?
    
    local_pubkey= File.open(local_pubkey_file, 'rb') { |f| f.read }
    local_pubkey.chomp!

    if  SSHKey.valid_ssh_public_key? local_pubkey
      fingerprint=  SSHKey.fingerprint local_pubkey
    else
      msg= "File #{local_pubkey} does not contains a valid ssh key"
      raise msg
    end

    password=Commands::Utils.ask_password("Enter password for #{remote_user}@#{remote_host}")
    rbox = Rye::Box.new(remote_host,:user => remote_user,:password=>password)
    
    str_home= rbox.pwd
    if  rbox.file_exists? ("#{str_home}"+"/.ssh/authorized_keys")
      
      str_authorized=rbox.string_download ("#{str_home}"+"/.ssh/authorized_keys")
      str_authorized_arr=str_authorized.lines

      str_authorized_arr.map! {|x| x.chomp } # remove end newline for each element

      result=str_authorized_arr.include?(local_pubkey)

      str_authorized_arr.select! { |key| # remove the selected key
        SSHKey.fingerprint(key) != fingerprint
      }

     applejack = StringIO.new "#{str_authorized_arr.join("\n")}"
     rbox.file_upload applejack, "#{str_home}"+"/.ssh/authorized_keys"
      
    end

    return result
    
  end

=begin
Elenca le chiavi pubbliche autorizzate al login automatico, leggendole dal file remoto ~/.ssh/authorized_keys.
=end
  def pri_listkeys(remote_host,remote_user)

    password=Commands::Utils.ask_password("Enter password for #{remote_user}@#{remote_host}")
    rbox = Rye::Box.new(remote_host,:user => remote_user,:password=>password)

    str_home= rbox.pwd

    if (!str_home.nil?) && rbox.file_exists?("#{str_home}"+"/.ssh/authorized_keys")

      str_authorized=rbox.string_download ("#{str_home}"+"/.ssh/authorized_keys")
      return str_authorized.lines
    else
      return nil
    end

  end

=begin
Restituisce un array associativo, del tipo:
chiave_pubblica_locale => fingerprint,
chiave_pubblica_locale => fingerprint,
...
=end
  def getLocalFingerprint
    localkeys=Hash.new
    Dir.glob(File.join(Rye.sysinfo.home, '.ssh', 'id*.pub')).each { |file|

      content=File.open(file, 'rb') { |f|
        f.read
      }.chomp

      if  SSHKey.valid_ssh_public_key? content
        fingerprint= SSHKey.fingerprint(content)
        localkeys[file]=fingerprint
      end
      
    }

    localkeys

  end

  end # end class SSHKeyCommand
  
end # end module Commands
