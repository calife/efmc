# require 'logging/logging'

module Commands

  class Dispatcher

    # Include this to access logger via logger.xxx("message") everywhere
    include Logging

    # attr_accessor :command, :global, :options, :args

    def initialize(arguments)
      @command, @global, @options, @args = arguments
    end

    def dispatch

      logger.debug("Start dispatching...")
      logger.debug("command #{@command}")
      logger.debug("with global #{@global}")
      logger.debug("with options #{@options}")
      logger.debug("with args #{@args}" )
      
      case @command
      when :snapshot_save # , :snapshot_restore
        Commands::BackupCommand.new([@command,@global, @options, @args]).execute        
      when :go2
        Commands::Go2Command.new([@command,@global, @options, @args]).execute
      when :ask4
        Commands::Ask4Command.new([@command,@global, @options, @args]).execute
      when :sshkey_add , :sshkey_remove , :sshkey_list
        Commands::SSHKeyCommand.new([@command,@global, @options, @args]).execute
      # when :tail
      #   Commands::TailCommand.new([:tail,@global, @options, @args]).execute
      when :network_scan
        Commands::NetworkCommand.new([@command,@global, @options, @args]).execute
      else
        msg="Command not valid: #{@command}"
        raise msg
      end

      logger.debug("...completed at #{Time.now}")

      logger.debug("...leave dispatching")
      
    end
  end

end
