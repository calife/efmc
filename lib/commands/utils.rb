module Commands

  module Utils

=begin
       Mostra il prompt di inserimento della password
=end  
    def self.ask_password(prompt='')
      pass = ask("#{prompt}:  ") { |q| q.echo = '*' }
      return pass
    end

=begin
       valid_ip_address
=end  
    def self.valid_ip_address?(ip_address)
      /^\d{,3}\.\d{,3}\.\d{,3}\.\d{,3}$/.match(ip_address)
    end

=begin
       valid_ip_network
=end  
    def self.valid_ip_network?(ip_network)
      /^\d{,3}\.\d{,3}\.\d{,3}\.\d{,3}\/\d{,3}$/.match(ip_network)
    end

=begin
       Restituisce un array con hostname e username
=end
    def self.decode_args(arg)
      username=arg.split('@')[0]
      host=arg.split('@')[1]
      return username,host
    end
    
  end

end
