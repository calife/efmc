# coding: utf-8
module Commands

  class NetworkCommand < Commands::BaseCommand

=begin
 http://stackoverflow.com/questions/9485441/method-to-find-available-devices-from-255-ip-address
=end    
    def network_scan

      logger.debug("Starting" + self.class.to_s + "___" + __method__.to_s+" ...")

      network=@args.first

      logger.debug(network)

      unless Commands::Utils.valid_ip_network?(network)
        msg= "#{network} is not a valid network/netmask range. Es. 172.16.1.0/24"
        raise msg
      end
      
      net1 = IPAddr.new network

      statuses=net1.to_range.map do |host|

        pid=fork {
          exec("ping", "-c 3", "#{host}",:out=>"/dev/null",err: :out)
        }

        [pid,host.to_s]
        
      end

      statuses.each { |p,h|
        
        Process.wait(p)

        sleep 0.10

        found=$?.exitstatus.zero?

        if @options.include?( :format )

          case @options[ :format ]
          when "csv"
            found_str=(found)?"Host #{h} found":"Host #{h} not found"
             printf("%s\n",found_str)
          when "pretty"
            printf("%s\n","Host found #{h}") if found
          else
                raise "Unknown format Exception"
          end

        end

      }

      logger.debug "Fine main process #{Process.pid}"
      logger.debug("...leaving" + self.class.to_s + "___" + __method__.to_s)      

    end
    
 
  end
  
end
