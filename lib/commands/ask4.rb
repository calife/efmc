module Commands

  class Ask4Command < Commands::BaseCommand

    def ask4()
      logger.debug("Starting" + self.class.to_s + "___" + __method__.to_s+" ...")
      
      appname=@args.first

      aliases = Commands::ALIASES_CMD.select { |key, value| key.to_s.match(/#{appname}/i) }
      if aliases.size>0

        aliases.each { |x,y|
          printf "%s => %s\n", x.to_s , y
        }

      else
        msg="Alias #{appname} not found."
        raise msg
      end
      logger.debug("...leaving" + self.class.to_s + "___" + __method__.to_s)
    end
    
  end
  
end
