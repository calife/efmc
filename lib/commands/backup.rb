module Commands

  class BackupCommand < Commands::BaseCommand

    @@REPORT_CHAR_COUNT=60
    @@REPORT_CHAR="-"
    @@EOL="\n"
    @@HOME_DIR=ENV["HOME"]
    
    def snapshot_save()

      db_name=@args.first
      
      if Commands::DATABASES.key?(db_name.to_sym) # Perform single backup

        choices = Commands::DATABASES.select { |key, value| key.to_s.match(db_name) }

        do_backup choices
        
      else
        printf "%s\n%s\n" , "Database name not found or no database name provided."  , "Usage: #{File.basename($PROGRAM_NAME)} --help"
      end
      
    end

    private
    def do_backup(h)

      date=Time.now.strftime("%Y-%m-%d")

      @@REPORT_CHAR_COUNT.times {  print "#{@@REPORT_CHAR}".upcase  }
      print "#{@@EOL}"
      h.each { |database,y|
        print "Nome: #{database} #{@@EOL}"
        print "Configurazione: #{@@EOL}"
        y.each { |prop,val|
          print "\t#{prop}: \t\t\t #{val}#{@@EOL}"
        }

        backup_file="#{@@HOME_DIR}/#{database}-#{date}.dmp"
        log_file="#{@@HOME_DIR}/#{database}-#{date}.log"
        owner=y[:schemas].join(",")

        cmd=" exp #{y[:username]}/#{y[:password]}@#{database} "
        cmd+=" owner=#{owner} "
        cmd+=" file=#{backup_file} "
        cmd+=" log=#{log_file} "
        cmd+=" statistics=none "

        system(cmd)

        print "#{@@EOL}"

      }

      @@REPORT_CHAR_COUNT.times {  print "#{@@REPORT_CHAR}".upcase  }
      print "#{@@EOL}"

    end    
    
  end
  
end
