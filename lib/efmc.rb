require 'efmc/version.rb'

require 'logging/logging'

# Add requires for other files you add to your project here, so
# you just need to require this one file in your bin file
require 'commands/dispatcher'
require 'commands/base'
require 'commands/constants'
require 'commands/utils'
require 'commands/backup'

# require 'net/ssh'
require 'commands/go2'
require 'commands/ask4'
require 'commands/sshkey'
require 'commands/network'

require 'rye'
require 'sshkey'
require 'highline/import'
require 'ipaddr'

require 'commands/shell'
