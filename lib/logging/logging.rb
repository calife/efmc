module Logging

  VERSION = '0.0.1'

  # Global, memoized, lazy initialized instance of a logger
  # def logger
  #   @logger ||= Logger.new(STDOUT)
  # end

  def logger
    @logger ||= setup_logger({:level=>:debug,:appender=>nil})
    
    @who=caller[0]
    @who=@who[@who.index("efmc"),@who.size   ] if @who
    
    return @logger
  end

  def setup_logger(config)

    level=config[:level]
    appender=config[:appender]

    @logger=Logger.new(STDOUT) unless appender

    @logger.formatter = lambda do |severity, datetime, progname, msg|
      "[#{datetime}] [#{@who}] [#{severity}] - #{msg}\n"
    end
   
    case level
    when :fatal
      @logger.level = Logger::FATAL
    when :error
      @logger.level = Logger::ERROR
    when :info
      @logger.level = Logger::INFO
    when :warn
      @logger.level = Logger::WARN
    when :debug
      @logger.level = Logger::DEBUG
    else
      @logger.level = Logger::UNKNOWN
    end

    @logger
  end 
  
end
