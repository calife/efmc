# Ensure we require the local version and not one we might have installed already
require File.join([File.dirname(__FILE__),'lib','efmc','version.rb'])
spec = Gem::Specification.new do |s| 
  s.name = 'efmc'
  s.version = Efmc::VERSION
  s.author = 'Pucci Marcello'
  s.email = 'marcello.pucci@efmnet.com'
  s.homepage = 'http://your.website.com'
  s.platform = Gem::Platform::RUBY
  s.summary = 'Efm Control application'
  s.files = `git ls-files`.split("
")
  s.require_paths << 'lib'
  s.has_rdoc = true
  s.extra_rdoc_files = ['README.rdoc','efmc.rdoc']
  s.rdoc_options << '--title' << 'efmc' << '--main' << 'README.rdoc' << '-ri'
  s.bindir = 'bin'
  s.executables << 'efmc'
  s.add_development_dependency('rake')
  s.add_development_dependency('rdoc')
  s.add_development_dependency('aruba')
  s.add_runtime_dependency('gli','2.13.1')

  s.add_development_dependency('ruby-termios')
  s.add_runtime_dependency('rye')
  s.add_runtime_dependency('sshkey')
  s.add_runtime_dependency('highline')
  
end
